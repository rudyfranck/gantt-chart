import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { GanttModule } from '@syncfusion/ej2-angular-gantt';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {  ResizeService, SortService, FilterService, SelectionService, ReorderService,
  EditService, DayMarkersService, ToolbarService } from '@syncfusion/ej2-angular-gantt';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    GanttModule,
    AppRoutingModule
  ],
  providers: [ResizeService, SortService, FilterService, SelectionService, ReorderService,
    EditService, DayMarkersService, ToolbarService],
  bootstrap: [AppComponent]
})
export class AppModule { }
