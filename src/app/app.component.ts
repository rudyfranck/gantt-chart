import { Component, OnInit, ViewChild,  } from '@angular/core';
import { editingData, editingResources } from './data';
import { GanttComponent } from '@syncfusion/ej2-angular-gantt';
import { ClickEventArgs } from '@syncfusion/ej2-angular-navigations';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  @ViewChild('ganttExcel')
  public ganttObj: GanttComponent;
  public data: object[];
  public resources: object[];
  public resourceFields: object ;
  public taskSettings: object;
  public timelineSettings: object;
  public gridLines: string;
  public labelSettings: object;
  public projectStartDate: Date;
  public projectEndDate: Date;
  public editSettings: object;
  public eventMarkers: object[];
  public toolbar: string[];
  public splitterSettings: object;
  public columns: object[];
  public ngOnInit(): void {
      this.data = editingData;
      this.taskSettings = {
          id: 'TaskID',
          name: 'TaskName',
          startDate: 'StartDate',
          endDate: 'EndDate',
          duration: 'Duration',
          progress: 'Progress',
          dependency: 'Predecessor',
          child: 'subtasks',
          notes: 'info',
          resourceInfo: 'resources'
      };
      this.resourceFields = {
          id: 'resourceId',
          name: 'resourceName'
      };
     
      this.editSettings = {
          allowAdding: true,
          allowEditing: true,
          allowDeleting: true,          
          allowTaskbarEditing: true,
          showDeleteConfirmDialog: true
      };
      this.toolbar = ['Add', 'Edit', 'Update', 'Delete', 'Cancel', 'ExpandAll', 'CollapseAll','ExcelExport', 'CsvExport', 'PdfExport',];
      this.columns =  [
          { field: 'TaskID', width:60 },
          { field: 'TaskName', headerText: 'Job Name', width: '250', clipMode: 'EllipsisWithTooltip' },
          { field: 'StartDate' },
          { field: 'EndDate' },
          { field: 'Duration' },
          { field: 'Progress' },
          { field: 'resources' },
          { field: 'Predecessor' }
      ];
      this.timelineSettings = {
          topTier: {
              unit: 'Week',
              format: 'MMM dd, y',
          },
          bottomTier: {
              unit: 'Day',
              count: 1
          },
      };
      this.gridLines = 'Both';
      this.labelSettings = {
          leftLabel: 'TaskName',
          rightLabel: 'resources'
      };
      this.projectStartDate= new Date('03/25/2020');
      this.projectEndDate= new Date('07/28/2020');
      this.eventMarkers = [
          { day: '4/17/2020', label: 'Project approval and kick-off' },
          { day: '5/3/2020', label: 'Foundation inspection' },
          { day: '6/7/2020', label: 'Site manager inspection' },
          { day: '7/16/2020', label: 'Property handover and sign-off' },
      ];

      this.resources = editingResources;
      this.splitterSettings = {
          columnIndex: 2
      };

   
  }
  toolbarClick(args?: ClickEventArgs): void {
    debugger;
    if (args.item.id === 'GanttExport_excelexport') {
        this.ganttObj.excelExport();
    }
    else if (args.item.id === "GanttExport_csvexport") {
        this.ganttObj.csvExport();
    }
    else if (args.item.id === "GanttExport_pdfexport") {
        this.ganttObj.pdfExport();
    }
}
} 
